# Visualised Bubble Sort
This repository contains the code for visualising bubble sort using the [SFML Library](https://www.sfml-dev.org/index.php).

## Files
- `Draw.cpp`: contains the definitions for functions related to rendering objects and window events.
- `Sort.cpp`: contains the logic behind peforming the bubble sort.
- `main.cpp`: instantiates the Draw class and calls its `pollEvents` method.

## Dependencies
- [Visual Studio 2019](https://visualstudio.microsoft.com/vs/).
- [SFML](https://www.sfml-dev.org/index.php) is *not* required as it is packaged with the Visual Studio project.

## Demo
The GIF below shows the code in action:

<img src="Resources/bubble_sort.gif" width="300" height="200"/>
