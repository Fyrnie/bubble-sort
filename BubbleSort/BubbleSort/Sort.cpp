#include "Sort.h"

Sort::Sort() {
	/*
	* Default constructor for the Sort class, required by C++11 and earlier compilers
	*/
	return;
}

Sort::Sort(int windowWidth, int numToSort) {
	/*
	* Actual constructor for the Sort class, which defines the number of values to generate
	* and initialises the random number generation
	* 
	* Keyword arguments:
	* windowWidth -- the width of the screen used to calculate the rectangle width
	* numToSort -- the number of entries to generate in the list that will be sorted
	*/
	this->vectorLen = numToSort;
	this->rectangleWidth = windowWidth / vectorLen;

	// initialises random seed prior to generating the random values
	srand(time(NULL));
	this->generateValues();
}

Sort::~Sort() {
	/*
	* Destructor for the Sort class
	*/
}

void Sort::generateValues() {
	/*
	* Initial generation of the list values, calls generateValues() to get a random number
	*/
	for (int idx = 0; idx < this->vectorLen; idx++) {
		values.push_back(generateValue());
	}
}

int Sort::generateValue() {
	/*
	* Generates a random value between 1 and 100 using stdlib.h and time.h
	* 
	* Returns:
	* A random value between 1 and 100, scaled by using modulo
	*/
	return rand() % 100 + 1;
}

bool Sort::shouldSwap(int firstVal, int secondVal) {
	/*
	* Checks whether two values should be swapped when sorting
	* 
	* Keyword arguments:
	* firstVal -- the current value in the list
	* secondVal -- the next value in the list to compare the current one against
	* 
	* Returns:
	* A boolean for whether the first value is greater than the second. In bubble
	* sort this means that they should be swapped
	*/
	if (firstVal > secondVal)
		return true;
	return false;
}

int Sort::getVectorLen() {
	/*
	* Returns the length of the list of values, used in the Draw class when first
	* creating the SFML rectangle shapes
	* 
	* Returns:
	* The number of values that are being sorted
	*/
	return this->vectorLen;
}

float Sort::getListEntry(int index) {
	/*
	* Gets the current value in the list
	* 
	* Keyword arguments:
	* index -- the current index that the loop is at
	* 
	* Returns:
	* The specified value in the list at the given index
	*/
	return values[index];
}

bool Sort::shouldSort(int index) {
	/*
	* Checks whether the two neighbour values should be swapped
	* 
	* Keyword arguments:
	* index -- the current index that the loop is at
	* 
	* Returns:
	* A boolean for whether the values should be sorted (swapped) or not
	*/
	int currentVal = this->values[index];
	int nextVal = this->values[index + 1];
	// after each full iteration the values are checked to see if another pass is needed
	if (this->shouldSwap(currentVal, nextVal)) {
		this->values[index] = nextVal;
		this->values[index + 1] = currentVal;
		return true;
	}
	return false;
}

bool Sort::doAnotherPass() {
	/*
	* Checks whether another 'pass' is needed. In bubble sort, many passes might be needed
	* to fully sort a list of values
	* 
	* Returns:
	* A boolean for whether another pass is needed or not
	*/
	for (int idx = 0; idx < this->values.size(); idx++) {
		// only needs to find one value that isn't sorted before it says to do another pass
		if (this->values[idx] > this->values[idx + 1]) {
			return true;
		}
	}
	return false;
}

float Sort::getRectWidth() {
	/*
	* Accessor for the rectangle width, used in generating rectangles in the Draw class
	* 
	* Returns:
	* The width of the generated rectangles
	*/
	return this->rectangleWidth;
}

void Sort::setRectWidth(float width) {
	/*
	* Mutator for the rectangle width
	* 
	* Keyword arguments:
	* width -- the specified width of the rectangle to use
	*/
	this->rectangleWidth = width;
}