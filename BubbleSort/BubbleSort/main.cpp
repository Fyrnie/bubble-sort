/*
* Bubble Sort visualised using SFML. Made for learning the SFML library
* 
* Created by Elena Thomas on 10-05-2021
*/

#include "Draw.h"

int main()
{
    // the argument passed is the number of values to sort
    Draw window = Draw(800);

    std::cout << "Commencing drawing to window" << std::endl;

    while (window.drawing()) {
        window.pollEvents();
    }

    return 0;
}