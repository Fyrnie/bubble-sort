#pragma once
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>

class Sort
{
private:
	// all the values to be sorted in the bubble sort
	std::vector<float> values;
	int vectorLen;
	float rectangleWidth;

	int generateValue();
	bool shouldSwap(int firstVal, int secondVal);

public:
	// default constructor for the compiler to use, is not described only defined
	Sort();
	Sort(int windowWidth, int numToSort);
	~Sort();

	void generateValues();
	int getVectorLen();
	float getListEntry(int index);
	bool shouldSort(int index);
	bool doAnotherPass();

	// accessors and mutator defs
	float getRectWidth();
	void setRectWidth(float width);
};

