#pragma once
#include "Sort.h"
#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>

class Draw
{
private:
	// const defs for the window
	const int WIDTH = 1280;
	const int HEIGHT = 720;
	const int FPS_LIMIT = 60;
	const std::string WINDOW_TITLE = "Visualised Bubble Sort";
	// const defs for the rectangles
	const sf::Color RECT_FILL = sf::Color::Cyan;

	sf::RenderWindow* window;
	sf::Event event;
	sf::VideoMode videoMode;
	std::vector<sf::RectangleShape> rectangles;

	Sort sortingAlgo;

public:
	Draw(int numToSort);
	~Draw();

	void initVars();
	void initWindow();
	void pollEvents();
	void render();
	void createRectangles();
	void drawRectangles();
	void updateRectangles();

	const bool drawing() const;
};

