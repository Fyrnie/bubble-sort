#include "Draw.h"

Draw::Draw(int numToSort) {
	/*
	* The constructor for the Draw class, where the variables are initialised
	* and the SFML RenderWindow is created. Also, the Sort class object is made here
	* to handle the logic behind drawing items to the window
	* 
	* Keyword arguments:
	* numToSort -- the number of values to be generated and then sorted
	*/
	this->initVars();
	this->initWindow();

	// setup the logic for the bubble sort
	this->sortingAlgo = Sort(WIDTH, numToSort);
	this->sortingAlgo.generateValues();
	this->createRectangles();
}

Draw::~Draw() {
	/* 
	* The destructor for the Draw class
	*/
	delete this->window;
}

void Draw::initVars() {
	/*
	* Initialises the class variables, mainly the window object reference as a null value
	*/
	this->window = nullptr;
}

void Draw::initWindow() {
	/*
	* Initialises the SFML window, setting its properties and framerate limit
	*/
	this->videoMode.height = HEIGHT;
	this->videoMode.width = WIDTH;
	this->window = new sf::RenderWindow(
		this->videoMode, WINDOW_TITLE, sf::Style::Titlebar | sf::Style::Close
	);

	this->window->setFramerateLimit(FPS_LIMIT);
}

void Draw::pollEvents() {
	/*
	* Checks whether a user prompted event has occurred, be this the window
	* closing or a key being pressed
	*/
	// rendering is called here as there is no need to wait for an event to sort values
	this->render();

	while (this->window->pollEvent(this->event)) {
		switch (this->event.type) {
			case sf::Event::Closed:
				this->window->close();
				break;
			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::Escape)
					this->window->close();
			break;
		}
	}
}

void Draw::render() {
	/*
	* Renders the rectangles to the window, updating them as required
	*/
	this->window->clear();
	if (sortingAlgo.doAnotherPass()) {
		this->updateRectangles();
		this->drawRectangles();
	}
	this->window->display();
}

void Draw::createRectangles() {
	/*
	* Creates all the rectangles for visualising the list values to be sorted
	*/
	std::cout << "Acquired rectangle width: " << sortingAlgo.getRectWidth() << std::endl;

	for (int idx = 0; idx < sortingAlgo.getVectorLen(); idx++) {
		sf::RectangleShape rectangle;
		rectangle.setFillColor(RECT_FILL);
		// offsets the drawn rectangle by its current index, and draws it at the bottom of the screen
		// this is because setPosition uses the top left corner of the rectangle
		rectangle.setPosition(sortingAlgo.getRectWidth() * idx, HEIGHT);
		// height has to be negative so it draws from the bottom up
		rectangle.setSize(sf::Vector2f(
			sortingAlgo.getRectWidth(),
			-(HEIGHT * (sortingAlgo.getListEntry(idx)/100)))
		);
		rectangles.push_back(rectangle);
	}
	std::cout << "Created all rectangles" << std::endl;
}

void Draw::drawRectangles() {
	/*
	* Draws the rectangles that are generated in createRectangles to the RenderWindow
	*/
	for (int idx = 0; idx < this->rectangles.size() - 1; idx++) {
		rectangles[idx].setPosition(sortingAlgo.getRectWidth() * idx, HEIGHT);
		this->window->draw(rectangles[idx]);
	}
}

void Draw::updateRectangles() {
	/*
	* Updates the rectangles with the latest sort information from the Sort class object
	* making use of the shouldSort method to then swap around the rectangle ordering
	* in the vector
	*/
	for (int idx = 0; idx < this->rectangles.size() - 1; idx++) {
		bool shouldSwap = sortingAlgo.shouldSort(idx);
		// mimicking the functionality in the sorting class but for the drawn items
		sf::RectangleShape currentRect = this->rectangles[idx];
		sf::RectangleShape nextRect = this->rectangles[idx + 1];
		if (shouldSwap) {
			this->rectangles[idx] = nextRect;
			this->rectangles[idx + 1] = currentRect;
		}
	}
}

const bool Draw::drawing() const {
	/*
	* Checks whether the window is still drawing items to it
	* 
	* Returns:
	* A boolean for whether the window is still open or not
	*/
	return this->window->isOpen();
}